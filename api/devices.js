const { name, version, description } = require('../package.json');
const devices = (req, res) => {
res.json({
    name,
    description,
    version,
    uptime: process.uptime()
})
}
module.export = devices